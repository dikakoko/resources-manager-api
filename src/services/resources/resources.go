package resources

import (
	"encoding/json"
	"fmt"
	validator "gitlab.com/pw-order-of-devs/go/go-validators"
	"resources-manager-api/src/model/pagination"
	"resources-manager-api/src/model/resources"
	"resources-manager-api/src/storage"
	"resources-manager-api/src/utils"
	ua "resources-manager-api/src/utils/api"
)

func GetAll(p pagination.Pagination) ua.Resp {
	if data, err := storage.GetProvider().GetAll(p); err != nil {
		return ua.Resp500(err)
	} else {
		return ua.Resp200(data, ua.ApplicationJson)
	}
}

func Get(id string) ua.Resp {
	if data, err := storage.GetProvider().Get(id); err != nil {
		if err == utils.NoSuchElementError {
			return ua.Resp404(err)
		} else {
			return ua.Resp500(err)
		}
	} else {
		return ua.Resp200(data, ua.ApplicationJson)
	}
}

func GetContent(id string) ua.Resp {
	if data, err := storage.GetProvider().GetContent(id); err != nil {
		if err == utils.NoSuchElementError {
			return ua.Resp404(err)
		} else {
			return ua.Resp500(err)
		}
	} else {
		if len(data) == 0 {
			return ua.Resp500(fmt.Errorf("file is malformed or empty"))
		} else {
			return ua.Resp200(data, ua.ApplicationOctetStream)
		}
	}
}

func UploadContent(id string, content []byte) ua.Resp {
	if len(content) == 0 {
		return ua.Resp400(fmt.Errorf("%v", "content is empty."))
	} else if resp, err := storage.GetProvider().UploadContent(id, content); err != nil {
		return ua.Resp500(err)
	} else {
		return ua.Resp200([]byte(fmt.Sprintf("%d", resp)), ua.TextPlain)
	}
}

func Create(data []byte) ua.Resp {
	var resource resources.FileMetadata
	if err := json.Unmarshal(data, &resource); err != nil {
		return ua.Resp400(err)
	} else if errors, ok := validator.Validate(resource); !ok {
		return ua.Resp400(fmt.Errorf("%v", errors))
	} else if resp, err := storage.GetProvider().Create(resource); err != nil {
		return ua.Resp500(err)
	} else if resp != nil {
		return ua.Resp201([]byte(resp.String()), ua.TextPlain)
	} else {
		return ua.Resp500(fmt.Errorf("cannot create the resource"))
	}
}

func Update(id string, data []byte) ua.Resp {
	var resource resources.FileMetadata
	if err := json.Unmarshal(data, &resource); err != nil {
		return ua.Resp400(err)
	} else if errors, ok := validator.Validate(resource); !ok {
		return ua.Resp400(fmt.Errorf("%v", errors))
	} else if resp, err := storage.GetProvider().Update(id, resource); err != nil {
		return ua.Resp500(err)
	} else {
		return ua.Resp200([]byte(fmt.Sprintf("%d", resp)), ua.TextPlain)
	}
}

func Delete(id string) ua.Resp {
	if data, err := storage.GetProvider().Del(id); err != nil {
		return ua.Resp500(err)
	} else {
		return ua.Resp200([]byte(fmt.Sprintf("%d", data)), ua.TextPlain)
	}
}
