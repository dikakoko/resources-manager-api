package utils

import (
	"fmt"
	"gitlab.com/pw-order-of-devs/go/go-validators"
	"os"
)

func LoadValidators() {
	Logger.Debug("loading validators ...")
	if value, ok := os.LookupEnv("VALIDATORS_PATH"); ok {
		if errors := go_validators.AddRulesFromPath(value); errors != nil {
			Logger.Error("failed loading validators.")
			Logger.Error("encountered errors:")
			for _, err := range errors {
				Logger.Error(fmt.Sprintf("%v", err))
			}
			os.Exit(1)
		}
		Logger.Debug("validators loaded")
	} else {
		Logger.Debug("no validators to load")
	}
}
