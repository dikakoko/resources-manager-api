package api

import (
	"github.com/gin-gonic/gin"
)

const (
	OK            = 200
	Created       = 201
	BadRequest    = 400
	NotFound      = 404
	InternalError = 500
)

const (
	ApplicationJson        = "application/json"
	ApplicationOctetStream = "application/octet-stream"
	TextPlain              = "text/plain"
)

type Resp struct {
	Item        any
	HttpStatus  int
	ContentType string
	Err         error
}

func Resp200(body any, contentType string) Resp {
	return Resp{Item: body, HttpStatus: OK, ContentType: contentType}
}

func Resp201(body any, contentType string) Resp {
	return Resp{Item: body, HttpStatus: Created, ContentType: contentType}
}

func Resp400(err error) Resp {
	return Resp{HttpStatus: BadRequest, ContentType: ApplicationJson, Err: err}
}

func Resp404(err error) Resp {
	return Resp{HttpStatus: NotFound, ContentType: ApplicationJson, Err: err}
}

func Resp500(err error) Resp {
	return Resp{HttpStatus: InternalError, ContentType: ApplicationJson, Err: err}
}

func MapApiResponse(c *gin.Context, r Resp) {
	if r.Err != nil {
		c.Data(r.HttpStatus, TextPlain, []byte(r.Err.Error()))
	} else {
		if r.ContentType == TextPlain {
			c.Data(r.HttpStatus, TextPlain, r.Item.([]byte))
		} else if r.ContentType == ApplicationOctetStream {
			c.Data(r.HttpStatus, ApplicationOctetStream, r.Item.([]byte))
		} else {
			c.JSON(r.HttpStatus, r.Item)
		}
	}
}
