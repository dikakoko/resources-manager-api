package server

import (
	"context"
	"fmt"
	gincors "github.com/gin-contrib/cors"
	ginzap "github.com/gin-contrib/zap"
	"github.com/gin-gonic/gin"
	"net/http"
	"os"
	"os/signal"
	"resources-manager-api/src/api"
	"resources-manager-api/src/helpers/discovery"
	"resources-manager-api/src/utils"
	"sync"
	"syscall"
	"time"
)

var lock = &sync.Mutex{}
var serverInstance *http.Server

func getInstance() *http.Server {
	lock.Lock()
	defer lock.Unlock()
	if serverInstance == nil {
		utils.Logger.Debug("Creating server instance")
		router := gin.New()
		router.Use(ginzap.GinzapWithConfig(utils.Logger, &ginzap.Config{
			TimeFormat: time.RFC3339,
			UTC:        true,
			SkipPaths:  []string{"/health"},
		}))
		if !utils.GetBoolEnvOrDefault("CORS_STRICT", true) {
			router.Use(gincors.New(gincors.Config{
				AllowOrigins:     []string{"*"},
				AllowHeaders:     []string{"*"},
				AllowMethods:     []string{"GET", "POST", "PUT", "DELETE"},
				ExposeHeaders:    []string{"Content-Length"},
				AllowCredentials: true,
				MaxAge:           12 * time.Hour,
			}))
		}
		router.Use(ginzap.RecoveryWithZap(utils.Logger, true))
		api.Health(router)
		api.V1(router)
		serverInstance = &http.Server{
			Addr:    serverAddr(),
			Handler: router,
		}
	} else {
		utils.Logger.Debug("Server instance already exists")
	}
	return serverInstance
}

func serverAddr() string {
	return fmt.Sprintf(
		"%s:%d",
		utils.GetEnvOrDefault("HOST", "0.0.0.0"),
		utils.GetIntEnvOrDefault("PORT", 8080),
	)
}

func StartServer() {
	gin.SetMode(utils.GetEnv("GIN_MODE"))
	go func() {
		if err := getInstance().ListenAndServe(); err != nil {
			utils.Logger.Debug(err.Error())
		}
	}()

	utils.Logger.Info("Server is listening at " + serverAddr())
}

func ShutdownServer() {
	if serverInstance == nil {
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := getInstance().Shutdown(ctx); err != nil {
		utils.Logger.Debug("Server forced to shutdown: " + err.Error())
	}
	utils.Logger.Info("Bye!")
}

func AwaitServerShutdown() {
	quit := make(chan os.Signal, 10)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	utils.Logger.Info("Shutting down server...")
	discovery.Deregister()
	ShutdownServer()
}
