package mysql

import (
	"fmt"
	"resources-manager-api/src/utils"
)

type DatabaseClient struct{}

func (DatabaseClient) ConnString() string {
	return fmt.Sprintf(
		"%s:%s@tcp(%s:%d)/%s",
		utils.GetEnv("MYSQL_USER"),
		utils.GetEnv("MYSQL_PASSWORD"),
		utils.GetEnvOrDefault("MYSQL_HOST", "localhost"),
		utils.GetIntEnvOrDefault("MYSQL_PORT", 3306),
		utils.GetEnv("MYSQL_DB"),
	)
}

func (DatabaseClient) Driver() string {
	return "mysql"
}
