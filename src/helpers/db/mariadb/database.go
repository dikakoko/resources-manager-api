package mariadb

import (
	"fmt"
	"resources-manager-api/src/utils"
)

type DatabaseClient struct{}

func (DatabaseClient) ConnString() string {
	return fmt.Sprintf(
		"%s:%s@tcp(%s:%d)/%s",
		utils.GetEnv("MARIADB_USER"),
		utils.GetEnv("MARIADB_PASSWORD"),
		utils.GetEnvOrDefault("MARIADB_HOST", "localhost"),
		utils.GetIntEnvOrDefault("MARIADB_PORT", 3306),
		utils.GetEnv("MARIADB_DB"),
	)
}

func (DatabaseClient) Driver() string {
	return "mysql"
}
