package discovery

import (
	"fmt"
	"github.com/go-resty/resty/v2"
	"resources-manager-api/src/utils"
)

type consulMeta struct {
	version string
}

type consulCheck struct {
	Name     string
	HTTP     string
	Interval string
	Timeout  string
}

type consulWeights struct {
	Passing int
	Warning int
}

type consulRegisterItem struct {
	ID                string
	Name              string
	Tags              []string
	Address           string
	Port              int
	EnableTagOverride bool
	Meta              consulMeta
	Check             consulCheck
	Weights           consulWeights
}

var client = resty.New()
var apiName = "resources-manager-api"

func consulRegister() {
	port := utils.GetIntEnvOrDefault("PORT", 8080)
	resp, err := client.R().
		SetBody(consulRegisterItem{
			ID:      apiName,
			Name:    apiName,
			Address: apiName,
			Port:    port,
			Tags: []string{
				"primary",
				"urlprefix-/api/v1/resources",
				"resources-manager",
				"v1",
			},
			EnableTagOverride: false,
			Meta: consulMeta{
				version: utils.GetEnv("VERSION"),
			},
			Check: consulCheck{
				Name:     "Resources Manager API HTTP check",
				HTTP:     fmt.Sprintf("http://%s:%d/health", apiName, port),
				Interval: "10s",
				Timeout:  "2s",
			},
			Weights: consulWeights{
				Passing: 10,
				Warning: 1,
			},
		}).
		Put(fmt.Sprintf("http://%s:8500/v1/agent/service/register", utils.GetEnvOrDefault("CONSUL_HOST", "consul")))
	if err != nil {
		utils.FatalError(fmt.Errorf("failed registering to discovery: %v", err))
	} else if resp.IsError() {
		utils.FatalError(fmt.Errorf("failed registering to discovery: %v", resp))
	} else {
		utils.Logger.Debug("successfully registered to discovery")
	}
}

func consulDeregister() {
	resp, err := client.R().
		Put(fmt.Sprintf("http://%s:8500/v1/agent/service/deregister/%s", utils.GetEnvOrDefault("CONSUL_HOST", "consul"), apiName))
	if err != nil {
		utils.FatalError(fmt.Errorf("failed deregistering to consul: %v", err))
	} else if resp.IsError() {
		utils.FatalError(fmt.Errorf("failed deregistering to consul: %v", resp))
	} else {
		utils.Logger.Debug("successfully deregistered from consul")
	}
}
