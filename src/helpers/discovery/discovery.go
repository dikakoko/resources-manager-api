package discovery

import "resources-manager-api/src/utils"

func consulEnabled() bool {
	return utils.GetBoolEnvOrDefault("CONSUL_ENABLED", false)
}

func Register() {
	if consulEnabled() {
		consulRegister()
	}
}

func Deregister() {
	if consulEnabled() {
		consulDeregister()
	}
}
