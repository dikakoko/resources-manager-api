package mariadb

const getAllQuery = `
select id, modification_date, creation_date, file_type, name,
   	description, author_id, size, duration, tags, time_pinpoints
from resource_provider.resources
limit ? offset ?
`

const getResourceQuery = `
select id, modification_date, creation_date, file_type, name,
	description, author_id, size, duration, tags, time_pinpoints
from resource_provider.resources
where id = ?
`

const getResourceContentQuery = `
select content
from resource_provider.resources
where id = ?
`

const uploadResourceContentQuery = `
update resource_provider.resources
set content = ?
where id = ?
`

const createResourceQuery = `
insert into resource_provider.resources (
	id, content, creation_date, file_type,
	name, description, author_id, size, duration, tags, time_pinpoints
) values (?, '', ?, ?, ?, ?, ?, ?, ?, ?, ?)
`

const updateResourceQuery = `
update resource_provider.resources
set modification_date = ?, file_type = ?, name = ?, description = ?,
	author_id = ?, size = ?, duration = ?, tags = ?, time_pinpoints = ?
where id = ?
`

const deleteResourceQuery = `
delete
from resource_provider.resources
where id = ?
`
