package shared

import (
	"github.com/gofrs/uuid"
	"github.com/lib/pq"
	"resources-manager-api/src/helpers/db"
	"resources-manager-api/src/model/pagination"
	mr "resources-manager-api/src/model/resources"
	"resources-manager-api/src/utils"
	"time"
)

type DefaultProvider struct {
	GetAllQuery                string
	GetResourceQuery           string
	GetResourceContentQuery    string
	UploadResourceContentQuery string
	CreateResourceQuery        string
	UpdateResourceQuery        string
	DeleteResourceQuery        string
}

func (mp DefaultProvider) GetAll(p pagination.Pagination) (mr.PaginatedResource, error) {
	return getResources(mp.GetAllQuery, p)
}

func (mp DefaultProvider) Get(id string) (*mr.Resource, error) {
	return getResource(mp.GetResourceQuery, id)
}

func (mp DefaultProvider) GetContent(id string) ([]byte, error) {
	return getResourceContent(mp.GetResourceContentQuery, id)
}

func (mp DefaultProvider) UploadContent(id string, content []byte) (int, error) {
	return uploadResourceContent(mp.UploadResourceContentQuery, id, content)
}

func (mp DefaultProvider) Create(metadata mr.FileMetadata) (*uuid.UUID, error) {
	return createResource(mp.CreateResourceQuery, metadata)
}

func (mp DefaultProvider) Update(id string, metadata mr.FileMetadata) (int, error) {
	return updateResource(mp.UpdateResourceQuery, id, metadata)
}

func (mp DefaultProvider) Del(id string) (int, error) {
	return deleteResource(mp.DeleteResourceQuery, id)
}

func getResourcesCount() (int, error) {
	var count int
	if err := db.GetDbClient().QueryRow(resourcesCount).Scan(&count); err != nil {
		utils.Logger.Error(err.Error())
		return -1, err
	} else {
		return count, nil
	}
}

func getResources(query string, p pagination.Pagination) (mr.PaginatedResource, error) {
	if total, err := getResourcesCount(); err != nil {
		return mr.PaginatedResource{Resources: nil, Pagination: p.WithTotal(-1)}, err
	} else {
		pages := p.WithTotal(total)
		rows, _ := db.GetDbClient().Query(query, p.PageSize, (p.PageNumber-1)*p.PageSize)
		resources := make([]mr.Resource, 0)
		for rows.Next() {
			var resource mr.Resource
			if err = rows.Scan(
				&resource.Id, &resource.ModificationDate, &resource.CreationDate,
				&resource.Metadata.FileType, &resource.Metadata.Name, &resource.Metadata.Description,
				&resource.Metadata.AuthorId, &resource.Metadata.Size, &resource.Metadata.Duration,
				pq.Array(&resource.Metadata.TagList), pq.Array(&resource.Metadata.TimePinpoints),
			); err != nil {
				utils.Logger.Error(err.Error())
				return mr.PaginatedResource{Pagination: pages}, err
			}
			resources = append(resources, resource)
		}
		return mr.PaginatedResource{Resources: resources, Pagination: pages}, nil
	}
}

func getResource(query, id string) (*mr.Resource, error) {
	var resource mr.Resource
	if err := db.GetDbClient().QueryRow(query, id).Scan(
		&resource.Id, &resource.ModificationDate, &resource.CreationDate,
		&resource.Metadata.FileType, &resource.Metadata.Name, &resource.Metadata.Description,
		&resource.Metadata.AuthorId, &resource.Metadata.Size, &resource.Metadata.Duration,
		pq.Array(&resource.Metadata.TagList), pq.Array(&resource.Metadata.TimePinpoints),
	); err != nil {
		utils.Logger.Error(err.Error())
		return nil, err
	} else {
		return &resource, nil
	}
}

func getResourceContent(query, id string) ([]byte, error) {
	var resource []byte
	if err := db.GetDbClient().QueryRow(query, id).Scan(&resource); err != nil {
		utils.Logger.Error(err.Error())
		return nil, err
	} else {
		return resource, nil
	}
}

func uploadResourceContent(query, id string, content []byte) (int, error) {
	if _, err := db.GetDbClient().Exec(query, content, id); err != nil {
		utils.Logger.Error(err.Error())
		return 0, err
	}
	return 1, nil
}

func createResource(query string, metadata mr.FileMetadata) (*uuid.UUID, error) {
	uid, _ := uuid.NewV4()
	if _, err := db.GetDbClient().Exec(query,
		uid.String(), time.Now().UTC().UnixMilli(), metadata.FileType,
		metadata.Name, metadata.Description,
		metadata.AuthorId, metadata.Size, metadata.Duration,
		pq.Array(metadata.TagList), mr.TimePinpointsToPqArray(metadata.TimePinpoints),
	); err != nil {
		utils.Logger.Error(err.Error())
		return nil, err
	}
	return &uid, nil
}

func updateResource(query, id string, metadata mr.FileMetadata) (int, error) {
	if _, err := db.GetDbClient().Exec(query,
		time.Now().UTC().UnixMilli(), metadata.FileType, metadata.Name,
		metadata.Description, metadata.AuthorId, metadata.Size, metadata.Duration,
		pq.Array(metadata.TagList), mr.TimePinpointsToPqArray(metadata.TimePinpoints), id,
	); err != nil {
		utils.Logger.Error(err.Error())
		return 0, err
	}
	return 1, nil
}

func deleteResource(query, id string) (int, error) {
	if _, err := db.GetDbClient().Exec(query, id); err != nil {
		utils.Logger.Error(err.Error())
		return 0, err
	}
	return 1, nil
}
