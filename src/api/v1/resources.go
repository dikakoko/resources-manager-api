package v1

import (
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"resources-manager-api/src/model/pagination"
	sr "resources-manager-api/src/services/resources"
	ua "resources-manager-api/src/utils/api"
)

func Resources(router *gin.RouterGroup) {
	resources := router.Group("/resources")
	{
		resources.GET("", getAll)
		resources.GET("/:id", get)
		resources.GET("/:id/download", getContent)
		resources.PUT("/:id/upload", uploadContent)
		resources.POST("", create)
		resources.PUT("/:id", update)
		resources.DELETE("/:id", del)
	}
}

func getAll(c *gin.Context) {
	ua.MapApiResponse(c, sr.GetAll(pagination.GetPagination(c)))
}

func get(c *gin.Context) {
	ua.MapApiResponse(c, sr.Get(c.Param("id")))
}

func getContent(c *gin.Context) {
	ua.MapApiResponse(c, sr.GetContent(c.Param("id")))
}

func uploadContent(c *gin.Context) {
	data, _ := ioutil.ReadAll(c.Request.Body)
	ua.MapApiResponse(c, sr.UploadContent(c.Param("id"), data))
}

func create(c *gin.Context) {
	data, _ := ioutil.ReadAll(c.Request.Body)
	ua.MapApiResponse(c, sr.Create(data))
}

func update(c *gin.Context) {
	data, _ := ioutil.ReadAll(c.Request.Body)
	ua.MapApiResponse(c, sr.Update(c.Param("id"), data))
}

func del(c *gin.Context) {
	ua.MapApiResponse(c, sr.Delete(c.Param("id")))
}
