package api

import (
	"github.com/gin-gonic/gin"
)

func health(c *gin.Context) {
	c.Data(200, "text/plain", []byte("ok"))
}

func Health(router *gin.Engine) {
	router.GET("/health", health)
}
