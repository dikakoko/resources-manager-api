# Resources Manager API

## Table of Contents

- [About](#about)
- [Environment Variables](#env)
- [Validators](#validators)
- [Providers](#providers)
- [Usage](#usage)

## About <a name = "about"></a>

Resource Manager API

## Environment Variables <a name="env"></a>

Create a .env file in the root of your project

```
touch .env # create a new .env file
vim .env # open the .env file in the vim
```

Supported formats are described [here](https://github.com/joho/godotenv/blob/master/README.md)

## Validators <a name="validators"></a>

The library [go-validators](https://gitlab.com/pw-order-of-devs/go/go-validators) is utilized for validating structures. \
To add validators, `VALIDATORS_PATH` env variable has to be set, leading to the directory with supported files containing validator definitions.

Example of yaml validator:
```yaml
---
validators:
  - structure: FileMetadata
    field: Name
    expressions:
      - "^\\s*(?:\\S\\s*){1,24}$"
      - "[a-zA-Z]+"
```

## Providers <a name="providers"></a>

Supported providers are:
- In memory provider [only for testing purposes, not recommended for production]
  - STORAGE_PROVIDER: in_memory
- Postgres provider
  - STORAGE_PROVIDER: postgres
  - POSTGRES_USER
  - POSTGRES_HOST
  - POSTGRES_PORT
  - POSTGRES_PASSWORD
  - POSTGRES_DB
- MariaDB provider
  - STORAGE_PROVIDER: mariadb
  - MARIADB_USER
  - MARIADB_HOST
  - MARIADB_PORT
  - MARIADB_PASSWORD
  - MARIADB_DB

## Usage <a name="usage"></a>

#### Go commands:

Before you start the project make sure you have golang installed on your system: \
```
go version
```

to build project:
```
go build
```

to add dependency:
```
go get -u <package>
```

to install linter:
```
curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin 
```

to run lint:
```
golangci-lint run
```

#### Dev makefile commands:

to build project:
```
make build
```

to run lint:
```
make lint
```

to start docker server:
```
make up
```

to stop docker server:
```
make down
```

to lookup docker logs:
```
make logs
```

to run tests:
```
make test-in_memory
make test-postgres
make test-mariadb
```
