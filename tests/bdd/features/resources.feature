Feature: Resources management test scenario

  Scenario: API operations return expected results
    When Create resource request is received
    Then Resource is created
    When Upload content request is received
    Then Resource content is uploaded
    When Update resource request is received
    Then Resource is updated
    When Get all resources request is received
    Then List of all resources is returned
    When Get resource by id request is received
    Then Resource is returned
    When Get resource content by id request is received
    Then Resource content is not empty
    When Delete resource request is received
    Then Resource is deleted
