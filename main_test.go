package main

import (
	"resources-manager-api/tests/bdd/steps/resources"
	"testing"

	"github.com/cucumber/godog"
)

func TestFeature(t *testing.T) {
	suite := godog.TestSuite{
		ScenarioInitializer: func(sc *godog.ScenarioContext) {
			resources.InitializeScenario(sc)
		},
		Options: &godog.Options{
			Format:   "pretty",
			Paths:    []string{"tests/bdd/features"},
			TestingT: t,
		},
	}

	if suite.Run() != 0 {
		t.Fatal("non-zero status returned, failed to run feature tests")
	}
}
