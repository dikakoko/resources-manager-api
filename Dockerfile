FROM golang:1.18-alpine as builder

RUN apk add --no-cache upx

WORKDIR /usr/local/go/src/resources-manager-api
COPY go.mod ./
COPY go.sum ./

RUN go mod download
COPY . .

RUN go build -ldflags "-s -w" -o /app/rm-api

RUN go get github.com/pwaller/goupx \
  && go install github.com/pwaller/goupx
RUN goupx /app/rm-api

FROM alpine:3.15 as runner

WORKDIR /app
COPY --from=builder /app/rm-api ./
ENV GIN_MODE release

ENTRYPOINT /app/rm-api
