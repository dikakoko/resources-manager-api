package main

import (
	"resources-manager-api/src/helpers/discovery"
	"resources-manager-api/src/helpers/server"
	"resources-manager-api/src/utils"
)

func main() {
	utils.LoadEnv()
	utils.LoadValidators()
	server.StartServer()
	discovery.Register()
	server.AwaitServerShutdown()
}
